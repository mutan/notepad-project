import { Injectable } from '@angular/core';
import{ HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class ServicesService {

  constructor(private http:HttpClient) { }


  regsdata(regs:any){
    return this.http.post('http://localhost:3000/register',regs)
  }
  redata(regs:any){
    return this.http.get('http://localhost:3000/register',regs)
  }

  pronam(a:any){
    return this.http.post('http://localhost:3000/project',a)
  }
    getnam(a:any){
    return this.http.get('http://localhost:3000/project',a)
  }
  update(id:any,a:any){
    console.log(id,a)
    return this.http.put('http://localhost:3000/project/'+id,a)
  }
}
