import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChatboxRoutingModule } from './chatbox-routing.module';
import { ChatboxComponent } from './chatbox.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [ChatboxComponent],
  imports: [
    CommonModule,
    ChatboxRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ChatboxModule { }
