import { Component, OnInit,EventEmitter,Output } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-chatbox',
  templateUrl: './chatbox.component.html',
  styleUrls: ['./chatbox.component.scss']
})
export class ChatboxComponent implements OnInit {
  notes :any;
  noted:any=[];
  dates:any;
  dateTime:any;
  @Output() dismiss = new EventEmitter();
  @Output() focusout = new EventEmitter();
 
    constructor(
    private formBuilder:FormBuilder,
    ) {
   }

  ngOnInit(): void {
  
     // USER CREATION
this.notes=this.formBuilder.group({
  notepad:'',
});

  }
  
  submit(){
    this.dates = new Date();
    var dd = this.dates.getFullYear()+'-'+(this.dates.getMonth()+1)+'-'+this.dates.getDate();
    var time = this.dates.getHours() + ":" + this.dates.getMinutes() + ":" + this.dates.getSeconds();
    this.dateTime = dd+' '+time;
// console.log(dd);
// console.log(time);
    // console.log(this.dateTime)
    // console.log(this.notes.value)
    this.noted.push(this.notes.value)
    // console.log(this.noted)
this.notes.setValue({notepad: ''});
  }
 

  onDismiss(event:any){
    this.dismiss.emit(event);
  }
  
  onFocusOut(event:any){
    this.focusout.emit(event)
  }
}
