import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ServicesService } from '../../services.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { analyzeAndValidateNgModules } from '@angular/compiler';

@Component({
  selector: 'app-groupup',
  templateUrl: './groupup.component.html',
  styleUrls: ['./groupup.component.scss']
})
export class GroupupComponent implements OnInit {
  list: any;
  message: any;
  users: any;
  namess: any = [];
  Array: any = [];
  selectsuser: any = [];
  response:any;
  constructor(private http: HttpClient, private service: ServicesService,
     private route: ActivatedRoute,
     private router: Router) { }

  ngOnInit(): void {

    // GET NAMR FROM REGISTER 
    this.message = this.route.snapshot.paramMap.get('title');
    this.list = JSON.parse(this.message);
    console.log(this.list)

    // GET USER FROM API
    var name
    this.service.redata(name).subscribe((r) => {
      console.log("send=>", r);
      this.users = r;
      console.log(this.users)

    },
    );
  }

// GROUP SELECTION
  selectuser(a: any, ary: any, ind: any) {
    // console.log(a.target.checked);
    // console.log(ary);
    // console.log(ind);
    if (a.target.checked == true) {
      this.selectsuser.push(ary);
    } else {
      var index = this.selectsuser.map((x: any) => {
        // console.log(x._id)
        return x._id;
      }).indexOf(ary._id);
      this.selectsuser.splice(index, 1);
      //   console.log(this.selectsuser);
      // console.log("no",ary._id)
    }
    // console.log(this.selectsuser)
  }

// UPDATE API METHOD
done(){
  console.log(this.list._id);
  var dat=this.selectsuser;
console.log(dat)
var obj={ users:dat }
this.service.update(this.list._id,obj).subscribe((r)=>{
  console.log("send=>",r);
 this.response=r;
  this.router.navigate(['mainpage/createproject/display']);
 
},);



}

}
