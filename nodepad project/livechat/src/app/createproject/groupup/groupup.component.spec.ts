import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupupComponent } from './groupup.component';

describe('GroupupComponent', () => {
  let component: GroupupComponent;
  let fixture: ComponentFixture<GroupupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GroupupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
