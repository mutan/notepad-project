import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateprojectComponent } from './createproject.component';
import { DisplayComponent } from './display/display.component';
import { GroupupComponent } from './groupup/groupup.component';

const routes: Routes = [
  {path:'',component:CreateprojectComponent,pathMatch: 'full'},
  {path:'display',component:DisplayComponent},
  {path:'groupup',component:GroupupComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateprojectRoutingModule { }
