import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateprojectRoutingModule } from './createproject-routing.module';
import { CreateprojectComponent } from './createproject.component';
import { DisplayComponent } from './display/display.component';
import { ReactiveFormsModule } from '@angular/forms';
import { GroupupComponent } from './groupup/groupup.component';


@NgModule({
  declarations: [CreateprojectComponent, DisplayComponent, GroupupComponent],
  imports: [
    CommonModule,
    CreateprojectRoutingModule,
    ReactiveFormsModule
  ]
})
export class CreateprojectModule { }
