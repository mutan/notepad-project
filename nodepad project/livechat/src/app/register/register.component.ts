import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators,FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import{ServicesService} from '../services.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
user:any=FormGroup;
response:any;
submitted = false;
loginsuccess:any;
usnam:any;
  constructor(private router: Router,private formBuilder:FormBuilder
    ,private http:HttpClient,private service:ServicesService) { }

  ngOnInit(): void {

      // LOGINPAGE BACK STOP
    
      this.loginsuccess=localStorage.getItem('success');
      console.log(this.loginsuccess);
      if(this.loginsuccess==null){
      
      }else{
        this.router.navigate(['mainpage']);
      
      }

        // USER CREATION
this.user=this.formBuilder.group({
  name:['',Validators.required],
});

  }
  get f() { return this.user.controls; }


regs(a:any){
console.log(a)
this.usnam=JSON.stringify(a);
  this.submitted=true;
// this.user.setValue({name: ''});
 
if(this.user.invalid){
  return;
}
else{
  console.log(this.user.value)

  var dat=this.user.value
  console.log(dat)
  this.service.regsdata(dat).subscribe((r)=>{
    console.log("send=>",r);
   this.response=r;
   if(r==true){
    this.router.navigate(['mainpage/createproject',{title:this.usnam}]);
this.user.setValue({name: ''});
localStorage.setItem('success','123');


   }
  
    
  },  
    );
  }

}




}
