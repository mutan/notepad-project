import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainpageComponent } from './mainpage/mainpage.component';
import{MainpageModule}from './mainpage/mainpage.module';
const routes: Routes = [
  {path:'',
  loadChildren:()=>import('./register/register.module').then(m=>m.RegisterModule)},
{path:'mainpage',
component:MainpageComponent,
children:[
  {path:'createproject',
loadChildren:()=>import('./createproject/createproject.module').then(m=>m.CreateprojectModule)},
{path:'chatbox',
loadChildren:()=>import('./chatbox/chatbox.module').then(m=>m.ChatboxModule)},
]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes),
    MainpageModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
