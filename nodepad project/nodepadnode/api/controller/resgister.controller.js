'use strict';


var mongoose = require('mongoose'),
  Task = mongoose.model('Tasks');

exports.list_all_tasks = function(req, res) {
  Task.find({}, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};




exports.create_a_task = function(req, res) {
    Task.findOne({name:req.body.name},function(err,result){
        if(err){
            res.json("something wentwrong")
        }else{
            if(result){
                res.json(false)
            }else{
                var new_task = new Task(req.body);
                new_task.save(function(err, task) {
                  if (err)
                    res.send(err);
                  res.json(true);
                });
            }
        }
    })
 
};


exports.read_a_task = function(req, res) {
  Task.findById(req.params.registerId, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};


exports.update_a_task = function(req, res) {
  Task.findOneAndUpdate({_id: req.params.registerId}, req.body, {new: true}, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};


exports.delete_a_task = function(req, res) {


  Task.remove({
    _id: req.params.registerId
  }, function(err, task) {
    if (err)
      res.send(err);
    res.json({ message: 'Task successfully deleted' });
  });
};
