'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var TaskSchema = new Schema({
  projectname: {
    type: String,
    required: 'Kindly enter the name of the task'
  },
  users:{
      type:Array
  },
  notepadi:{
    type:Array
  },
  createdby:{
      type:String,
  },
  Created_date: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model('Project', TaskSchema);