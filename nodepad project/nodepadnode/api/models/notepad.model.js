'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var TaskSchema = new Schema({
  taskname: {
    type: String,
    required: 'Kindly enter the name of the task'
  },
  notepad:{
      type:Array
  },
  createdby:{
      type:String,
  },
  Created_date: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model('Notepad', TaskSchema);